function errbardata=calc_sem_morey(indata)

sub_means = mean(indata,2);
n_conds = size(indata,2);
indata = indata - repmat(sub_means,1,n_conds) + mean(sub_means);
var_tmp = var( indata );
var_tmp = var_tmp * n_conds / (n_conds - 1);
std_tmp = sqrt(var_tmp);
errbardata = std_tmp / sqrt(size(indata,1));
