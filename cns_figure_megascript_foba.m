function cns_figure_megascript_foba
% NOTE: Run with Matlab 2017b (or around that -- but not 2015b) for all
% figure stuff to work!

addpath('shadedErrorBar-master');
n_tps = 121; %AFTER TRIMMING TO ACCOUNT FOR BOLD SHIFT, IN THIS ANALYSIS
tp_bold_shift = 10; %also new to foba; see line above (10 is really shift of 5 in each direction)
transpose_double_everything = true; %MAYBE DO DIFFERENT FOR FOBA B/C AXES ACTUALLY MEANINGFUL? OR MAYBE NOT...

% needs to be in order preds, preds_bin, corrs, eucs, or everything goes to hell
fields_to_read = {'preds_%s_same', 'preds_%s_same_bin', 'corrs_%s', 'eucs_%s'};
outname='cns_figure_megascript_data_transposedouble_TRUE_%s.mat';

combo_codes_to_run = {'foba'};
current_combo_code = 1;

winsize_for_windowed_samediff = 30; % plus and minus

infiles = {...
            'sky1775_results_summary_compare_corr_euclid_FIXED3.mat'
            'sky1783_results_summary_compare_corr_euclid_FIXED3.mat'
            'sky1784_results_summary_compare_corr_euclid_FIXED3.mat'
            'sky1786_results_summary_compare_corr_euclid_FIXED3.mat'
            'sky1787_results_summary_compare_corr_euclid_FIXED3.mat'
            'sky1789_results_summary_compare_corr_euclid_FIXED3.mat'
            'sky1793_results_summary_compare_corr_euclid_FIXED3.mat'
            'sky1800_results_summary_compare_corr_euclid_FIXED3.mat'
          };
base_dir = '.';

n_subs = numel(infiles);
for i = 1:n_subs
    fprintf('loading subject %.2d...\n',i);
    dat(i) = load(fullfile(base_dir,infiles{i}));
end

fig_handles = [];

for i = 1:numel(fields_to_read)
    fields_to_read{i} = sprintf(fields_to_read{i}, combo_codes_to_run{current_combo_code});
end
outname = sprintf(outname,combo_codes_to_run{current_combo_code});

%calculate "best guess"
for i = 1:n_subs
    % get basic calcs for preds
    these_scores = getfield( dat(i), fields_to_read{1} );
    
    %new to foba
    for j = 1:numel(these_scores)
        these_scores{j} = flipud(these_scores{j});
        these_scores{j} = these_scores{j}(1+tp_bold_shift:end,1:end-tp_bold_shift);
    end
    
    if transpose_double_everything
        these_scores = do_transpose_double( these_scores );
    end
    windowed_samediff_ptcpreds{i} = do_windowed_samediff( these_scores, winsize_for_windowed_samediff );
    conf_mats_ptcpreds{i} = mrj_cellmean( these_scores );
    ptcpreds_vals_on_diag(i,:) = conf_mats_ptcpreds{i}(logical(eye(n_tps)));
    ptcpreds_vals_off_diag(i,:) = conf_mats_ptcpreds{i}(~logical(eye(n_tps)));
    offdiag_timeline_temp = conf_mats_ptcpreds{i};
    offdiag_timeline_temp(logical(eye(n_tps)))=NaN;
    ptcpreds_vals_off_diag_timeline(i,:) = nanmean(offdiag_timeline_temp); %warning: may be kinda weird if transpose_double_everything is false
    [~,~,conf_mats_ranked_ptcpreds{i}] = unique( conf_mats_ptcpreds{i} ); %highest is most similar
    conf_mats_ranked_ptcpreds{i} = (conf_mats_ranked_ptcpreds{i} - 1)  ./ max( conf_mats_ranked_ptcpreds{i} ); % scale to 0-1
    conf_mats_ranked_ptcpreds{i} = reshape( conf_mats_ranked_ptcpreds{i}, n_tps, n_tps );
    [pseudo_roc_ptcpreds_fps(i,:), pseudo_roc_ptcpreds_tps(i,:)] = do_pseudo_roc( conf_mats_ranked_ptcpreds{i} );
    pseudo_auc_ptcpreds(i) = auroc( sort(pseudo_roc_ptcpreds_tps(i,:))', sort(pseudo_roc_ptcpreds_fps(i,:))' );
    [actual_ptcpreds{i}, predicted_ptcpreds{i}] = do_best_guess_calcs( these_scores );
    pred_err_ptcpreds{i} = predicted_ptcpreds{i}-actual_ptcpreds{i};
    pred_err_ptcpreds_means(i) = mean(abs(pred_err_ptcpreds{i}));
    hc_ptcpreds{i} = histcounts(abs(pred_err_ptcpreds{i}), (0:n_tps) - .5 );
    hc_signed_ptcpreds{i} = histcounts(predicted_ptcpreds{i}-actual_ptcpreds{i}, (-n_tps+.5):(n_tps-.5) );
    
    % get basic calcs for preds_bin
    these_scores = getfield( dat(i), fields_to_read{2} );
    
    %new to foba
    for j = 1:numel(these_scores)
        these_scores{j} = flipud(these_scores{j});
        these_scores{j} = these_scores{j}(1+tp_bold_shift:end,1:end-tp_bold_shift);
    end
    
    if transpose_double_everything
        these_scores = do_transpose_double( these_scores );
    end
    windowed_samediff_ptcpreds_bin{i} = do_windowed_samediff( these_scores, winsize_for_windowed_samediff );
    conf_mats_ptcpreds_bin{i} = mrj_cellmean( these_scores );
    ptcpreds_bin_vals_on_diag(i,:) = conf_mats_ptcpreds_bin{i}(logical(eye(n_tps)));
    ptcpreds_bin_vals_off_diag(i,:) = conf_mats_ptcpreds_bin{i}(~logical(eye(n_tps)));
    offdiag_timeline_temp = conf_mats_ptcpreds_bin{i};
    offdiag_timeline_temp(logical(eye(n_tps)))=NaN;
    ptcpreds_bin_vals_off_diag_timeline(i,:) = nanmean(offdiag_timeline_temp); %warning: may be kinda weird if transpose_double_everything is false
    [~,~,conf_mats_ranked_ptcpreds_bin{i}] = unique( conf_mats_ptcpreds_bin{i} ); %highest is most similar
    conf_mats_ranked_ptcpreds_bin{i} = (conf_mats_ranked_ptcpreds_bin{i} - 1)  ./ max( conf_mats_ranked_ptcpreds_bin{i} ); % scale to 0-1
    conf_mats_ranked_ptcpreds_bin{i} = reshape( conf_mats_ranked_ptcpreds_bin{i}, n_tps, n_tps );
    [pseudo_roc_ptcpreds_bin_fps(i,:), pseudo_roc_ptcpreds_bin_tps(i,:)] = do_pseudo_roc( conf_mats_ranked_ptcpreds_bin{i} );
    pseudo_auc_ptcpreds_bin(i) = auroc( sort(pseudo_roc_ptcpreds_bin_tps(i,:))', sort(pseudo_roc_ptcpreds_bin_fps(i,:))' );
    [actual_ptcpreds_bin{i}, predicted_ptcpreds_bin{i}] = do_best_guess_calcs( these_scores );
    pred_err_ptcpreds_bin{i} = predicted_ptcpreds_bin{i}-actual_ptcpreds_bin{i};
    pred_err_ptcpreds_bin_means(i) = mean(abs(pred_err_ptcpreds_bin{i}));
    hc_ptcpreds_bin{i} = histcounts(abs(pred_err_ptcpreds_bin{i}), (0:n_tps) - .5 );
    hc_signed_ptcpreds_bin{i} = histcounts(predicted_ptcpreds_bin{i}-actual_ptcpreds_bin{i}, (-n_tps+.5):(n_tps-.5) );
    
    % get basic calcs for corrs (atanh)
    these_scores = getfield( dat(i), fields_to_read{3} );
    
    %new to foba
    for j = 1:numel(these_scores)
        these_scores{j} = flipud(these_scores{j});
        these_scores{j} = these_scores{j}(1+tp_bold_shift:end,1:end-tp_bold_shift);
    end
        
    if transpose_double_everything
        these_scores = do_transpose_double( these_scores );
    end
    for j = 1:numel(these_scores)
        these_scores{j} = atanh( these_scores{j} );
    end
    windowed_samediff_corrpreds{i} = do_windowed_samediff( these_scores, winsize_for_windowed_samediff );
    conf_mats_corrpreds{i} = mrj_cellmean( these_scores );
    corrpreds_vals_on_diag(i,:) = conf_mats_corrpreds{i}(logical(eye(n_tps)));
    corrpreds_vals_off_diag(i,:) = conf_mats_corrpreds{i}(~logical(eye(n_tps)));
    offdiag_timeline_temp = conf_mats_corrpreds{i};
    offdiag_timeline_temp(logical(eye(n_tps)))=NaN;
    corrpreds_vals_off_diag_timeline(i,:) = nanmean(offdiag_timeline_temp); %warning: may be kinda weird if transpose_double_everything is false
    [~,~,conf_mats_ranked_corrpreds{i}] = unique( conf_mats_corrpreds{i} ); %highest is most similar
    conf_mats_ranked_corrpreds{i} = (conf_mats_ranked_corrpreds{i} - 1)  ./ max( conf_mats_ranked_corrpreds{i} ); % scale to 0-1
    conf_mats_ranked_corrpreds{i} = reshape( conf_mats_ranked_corrpreds{i}, n_tps, n_tps );
    [pseudo_roc_corrpreds_fps(i,:), pseudo_roc_corrpreds_tps(i,:)] = do_pseudo_roc( conf_mats_ranked_corrpreds{i} );
    pseudo_auc_corrpreds(i) = auroc( sort(pseudo_roc_corrpreds_tps(i,:))', sort(pseudo_roc_corrpreds_fps(i,:))' );
    [actual_corrpreds{i}, predicted_corrpreds{i}] = do_best_guess_calcs( these_scores );
    pred_err_corrpreds{i} = predicted_corrpreds{i}-actual_corrpreds{i};
    pred_err_corrpreds_means(i) = mean(abs(pred_err_corrpreds{i}));
    hc_corrpreds{i} = histcounts(abs(pred_err_corrpreds{i}), (0:n_tps) - .5 );
    hc_signed_corrpreds{i} = histcounts(predicted_corrpreds{i}-actual_corrpreds{i}, (-n_tps+.5):(n_tps-.5) );
    
    % get basic calcs for Eucs (sign-flipped)
    these_scores = getfield( dat(i), fields_to_read{4} );
    
    %new to foba
    for j = 1:numel(these_scores)
        these_scores{j} = flipud(these_scores{j});
        these_scores{j} = these_scores{j}(1+tp_bold_shift:end,1:end-tp_bold_shift);
    end
    
    if transpose_double_everything
        these_scores = do_transpose_double( these_scores );
    end
    for j = 1:numel(these_scores)
        these_scores{j} = -1  * these_scores{j};
    end
    windowed_samediff_eucspreds{i} = do_windowed_samediff( these_scores, winsize_for_windowed_samediff );
    conf_mats_eucspreds{i} = mrj_cellmean( these_scores );
    eucspreds_vals_on_diag(i,:) = conf_mats_eucspreds{i}(logical(eye(n_tps)));
    eucspreds_vals_off_diag(i,:) = conf_mats_eucspreds{i}(~logical(eye(n_tps)));
    offdiag_timeline_temp = conf_mats_eucspreds{i};
    offdiag_timeline_temp(logical(eye(n_tps)))=NaN;
    eucspreds_vals_off_diag_timeline(i,:) = nanmean(offdiag_timeline_temp); %warning: may be kinda weird if transpose_double_everything is false
    [~,~,conf_mats_ranked_eucspreds{i}] = unique( conf_mats_eucspreds{i} ); %highest is most similar
    conf_mats_ranked_eucspreds{i} = (conf_mats_ranked_eucspreds{i} - 1)  ./ max( conf_mats_ranked_eucspreds{i} ); % scale to 0-1
    conf_mats_ranked_eucspreds{i} = reshape( conf_mats_ranked_eucspreds{i}, n_tps, n_tps );
    [pseudo_roc_eucspreds_fps(i,:), pseudo_roc_eucspreds_tps(i,:)] = do_pseudo_roc( conf_mats_ranked_eucspreds{i} );
    pseudo_auc_eucspreds(i) = auroc( sort(pseudo_roc_eucspreds_tps(i,:))', sort(pseudo_roc_eucspreds_fps(i,:))' );
    [actual_eucspreds{i}, predicted_eucspreds{i}] = do_best_guess_calcs( these_scores );
    pred_err_eucspreds{i} = predicted_eucspreds{i}-actual_eucspreds{i};
    pred_err_eucspreds_means(i) = mean(abs(pred_err_eucspreds{i}));
    hc_eucspreds{i} = histcounts(abs(pred_err_eucspreds{i}), (0:n_tps) - .5 );
    hc_signed_eucspreds{i} = histcounts(predicted_eucspreds{i}-actual_eucspreds{i}, (-n_tps+.5):(n_tps-.5) );
    
end


% SIMILARITY MATRICES - RANK UNITS
% preds_bin
fig_handles(end+1)=figure; imagesc( mrj_cellmean(conf_mats_ranked_ptcpreds_bin), [0 1] ); colorbar;

% corrs
fig_handles(end+1)=figure; imagesc( mrj_cellmean(conf_mats_ranked_corrpreds), [0 1] ); colorbar;

% Eucs
fig_handles(end+1)=figure; imagesc( mrj_cellmean(conf_mats_ranked_eucspreds), [0 1] ); colorbar;





% SIGNED HISTOGRAMS

% preds_bin
mean_hc_signed_ptcpreds_bin = mrj_cellmean(hc_signed_ptcpreds_bin);
fig_handles(end+1)=figure;
bar( (-n_tps+1):(n_tps-1),mean_hc_signed_ptcpreds_bin);
hold on;
er=errorbar((-n_tps+1):(n_tps-1),mean_hc_signed_ptcpreds_bin,std(vertcat(hc_signed_ptcpreds_bin{:}))/sqrt(n_subs),'k'); axis tight;
er.LineWidth=0.5;
er.CapSize=0;
er.LineStyle='none';
set(gca,'ticklength',[0 0])
xlim([-186 186]);
ylim([0 18]);

% corrs
mean_hc_signed_corrpreds = mrj_cellmean(hc_signed_corrpreds);
fig_handles(end+1)=figure;
bar( (-n_tps+1):(n_tps-1),mean_hc_signed_corrpreds);
hold on;
er=errorbar((-n_tps+1):(n_tps-1),mean_hc_signed_corrpreds,std(vertcat(hc_signed_corrpreds{:}))/sqrt(n_subs),'k'); axis tight;
er.LineWidth=0.5;
er.CapSize=0;
er.LineStyle='none';
set(gca,'ticklength',[0 0])
xlim([-186 186]);
ylim([0 18]);

% Eucs
mean_hc_signed_eucspreds = mrj_cellmean(hc_signed_eucspreds);
fig_handles(end+1)=figure;
bar( (-n_tps+1):(n_tps-1),mean_hc_signed_eucspreds);
hold on;
er=errorbar((-n_tps+1):(n_tps-1),mean_hc_signed_eucspreds,std(vertcat(hc_signed_eucspreds{:}))/sqrt(n_subs),'k'); axis tight;
er.LineWidth=0.5;
er.CapSize=0;
er.LineStyle='none';
set(gca,'ticklength',[0 0])
xlim([-186 186]);
ylim([0 18]);



% windowed samediff preds_bin
windowed_samediff_ptcpreds_bin_cat=vertcat(windowed_samediff_ptcpreds_bin{:});
fig_handles(end+1)=figure;
% plot(-winsize_for_windowed_samediff:winsize_for_windowed_samediff,mean(windowed_samediff_ptcpreds_bin_cat),'linewidth',2);
shadedErrorBar(-winsize_for_windowed_samediff:winsize_for_windowed_samediff, mean(windowed_samediff_ptcpreds_bin_cat), calc_sem_morey(windowed_samediff_ptcpreds_bin_cat), 'lineprops', {'linewidth',2} );
xlim([-winsize_for_windowed_samediff, winsize_for_windowed_samediff]);

% windowed samediff corrs
windowed_samediff_corrpreds_cat=vertcat(windowed_samediff_corrpreds{:});
fig_handles(end+1)=figure;
% plot(-winsize_for_windowed_samediff:winsize_for_windowed_samediff,mean(windowed_samediff_corrpreds_cat),'linewidth',2);
shadedErrorBar(-winsize_for_windowed_samediff:winsize_for_windowed_samediff, mean(windowed_samediff_corrpreds_cat), calc_sem_morey(windowed_samediff_corrpreds_cat), 'lineprops', {'linewidth',2} );
xlim([-winsize_for_windowed_samediff, winsize_for_windowed_samediff]);

% windowed samediff Eucs
windowed_samediff_eucspreds_cat=vertcat(windowed_samediff_eucspreds{:});
fig_handles(end+1)=figure;
% plot(-winsize_for_windowed_samediff:winsize_for_windowed_samediff,mean(windowed_samediff_eucspreds_cat),'linewidth',2);
shadedErrorBar(-winsize_for_windowed_samediff:winsize_for_windowed_samediff, mean(windowed_samediff_eucspreds_cat), calc_sem_morey(windowed_samediff_eucspreds_cat), 'lineprops', {'linewidth',2} );
xlim([-winsize_for_windowed_samediff, winsize_for_windowed_samediff]);



% TIMELINES OF ON- AND OFF-DIAG MEANS (AND THE DIFFS OF THOSE)

% preds_bin
fig_handles(end+1)=figure; hold on;
plot(mean(ptcpreds_bin_vals_on_diag),'b','linewidth',1.5);
plot(mean(ptcpreds_bin_vals_off_diag_timeline),'b:','linewidth',1.5);
% plot(mean(ptcpreds_bin_vals_on_diag)-mean(ptcpreds_bin_vals_off_diag_timeline),'k','linewidth',2);

% corrs
fig_handles(end+1)=figure; hold on;
plot(mean(corrpreds_vals_on_diag),'b','linewidth',1.5);
plot(mean(corrpreds_vals_off_diag_timeline),'b:','linewidth',1.5);
% plot(mean(corrpreds_vals_on_diag)-mean(corrpreds_vals_off_diag_timeline),'k','linewidth',2);

% Eucs
fig_handles(end+1)=figure; hold on;
plot(mean(eucspreds_vals_on_diag),'b','linewidth',1.5);
plot(mean(eucspreds_vals_off_diag_timeline),'b:','linewidth',1.5);
% plot(mean(eucspreds_vals_on_diag)-mean(eucspreds_vals_off_diag_timeline),'k','linewidth',2);



% PSEUDO-ROC CURVES -- TOO WEIRD TO USE BUT INTERDASTING?


% preds_bin
fig_handles(end+1)=figure; hold on;
plot(mean(pseudo_roc_ptcpreds_bin_fps), mean(pseudo_roc_ptcpreds_bin_tps),'linewidth',2);
axis image; plot([0 1], [0 1], 'k:' );

% corrs
fig_handles(end+1)=figure; hold on;
plot(mean(pseudo_roc_corrpreds_fps), mean(pseudo_roc_corrpreds_tps),'linewidth',2);
axis image; plot([0 1], [0 1], 'k:' );

% Eucs
fig_handles(end+1)=figure; hold on;
plot(mean(pseudo_roc_eucspreds_fps), mean(pseudo_roc_eucspreds_tps),'linewidth',2);
axis image; plot([0 1], [0 1], 'k:' );



if ~isempty(outname)
    clear dat;
    save(outname);
end

keyboard;





function [fps, tps] = do_pseudo_roc( rank_mat )
% assume always goes from 0-1
% also assume we're always just gonna plot in .001 increments

mat_dim = size(rank_mat,1);
tp_inds = logical(eye(mat_dim));
fp_inds = ~tp_inds;
total_tp = sum(tp_inds(:));
total_fp = sum(fp_inds(:));
tp_vals = rank_mat(tp_inds);
fp_vals = rank_mat(fp_inds);

tps = zeros(1001,1);
fps = tps;
criteria_vals = 0:.001:1;

for i = 1:numel(criteria_vals)
    this_critval = criteria_vals(i);
    this_tp_count = sum(tp_vals>=this_critval);
    this_fp_count = sum(fp_vals>=this_critval);
    
    fps(i) = this_fp_count / total_fp;
    tps(i) = this_tp_count / total_tp;
end



function ws = do_windowed_samediff( scores_cell, winsize )

n_cells = numel(scores_cell);
cell_avgs = nan(n_cells, 2*winsize+1);
n_tps = size(scores_cell{1},1);
start_ind = winsize+1;
end_ind = n_tps - winsize;
n_tps = n_tps - 2*winsize;

for i = 1:n_cells
    these_scores = scores_cell{i};
    these_wins = nan( n_tps, 2*winsize+1 );
    wins_ind = 1;
    for j = start_ind:end_ind
        this_start = j - winsize;
        this_end = j + winsize;
        these_wins(wins_ind,:) = these_scores(j,this_start:this_end);
        wins_ind = wins_ind + 1;
    end
    cell_avgs(i,:) = mean(these_wins);
end
ws = mean(cell_avgs);


function [actual, predicted] = do_best_guess_calcs( scores )

scores = mrj_cellmean(scores);
n_tps = size(scores,1);

for i = 1:n_tps
    [~,~,ranks(:,i)] = unique(scores(:,i));
end

for i = 1:n_tps
    ranks_smooth(i,:) = smooth(ranks(i,:));
end

ranks_for_guessing = ranks_smooth;
is = nan(n_tps,1);
js = is;
for i = 1:n_tps
    [is(i),js(i)] = find(ranks_for_guessing==max(ranks_for_guessing(:)),1,'first');
    ranks_for_guessing(is(i),:)=0;
    ranks_for_guessing(:,js(i))=0;
end

actual=is;
predicted=js;


function out_cell = do_transpose_double( in_cell )

n_in = numel(in_cell);
out_cell=cell( n_in*2, 1 );
for i = 1:n_in
    out_cell{i} = in_cell{i};
    out_cell{i+n_in} = in_cell{i}';
end


function cellmean = mrj_cellmean( vals )

if iscell(vals)
    sumvals = zeros(size(vals{1}));
    for i = 1:numel(vals)
        sumvals = sumvals + vals{i};
    end
    
    cellmean = sumvals ./ numel(vals);
else
    cellmean = vals;
end
