res = '-r600';
fontsize=28;


figure(fig_handles(1));
xlabel('Time (s)');
ylabel('Time (s)');
axis square; box on;
set(gca,'fontsize',fontsize);
xticks(0:40:130);
yticks(0:40:130);
xlim([0 132]);
ylim([0 132]);
xticklabels({'0' '40' '80' '120'});
yticklabels({'0' '40' '80' '120'});
print('simrank_fofo_predsbin.png','-dpng',res);
mrj_png_trans('simrank_fofo_predsbin.png');

figure(fig_handles(2));
xlabel('Time (s)');
ylabel('Time (s)');
axis square; box on;
set(gca,'fontsize',fontsize);
xticks(0:40:130);
yticks(0:40:130);
xlim([0 132]);
ylim([0 132]);
xticklabels({'0' '40' '80' '120'});
yticklabels({'0' '40' '80' '120'});
print('simrank_fofo_corrs.png','-dpng',res);
mrj_png_trans('simrank_fofo_corrs.png');

figure(fig_handles(3));
xlabel('Time (s)');
ylabel('Time (s)');
axis square; box on;
set(gca,'fontsize',fontsize);
xticks(0:40:130);
yticks(0:40:130);
xlim([0 132]);
ylim([0 132]);
xticklabels({'0' '40' '80' '120'});
yticklabels({'0' '40' '80' '120'});
print('simrank_fofo_eucs.png','-dpng',res);
mrj_png_trans('simrank_fofo_eucs.png');



figure(fig_handles(4));
xlabel('Prediction error (s)');
ylabel('Frequency');
box on;
set(gca,'fontsize',fontsize);
set(gcf,'paperposition',[0 0 18 3]);
yticks(0:6:18);
print('prederror_histo_fofo_predsbin.png','-dpng',res);
mrj_png_trans('prederror_histo_fofo_predsbin.png');

figure(fig_handles(5));
xlabel('Prediction error (s)');
ylabel('Frequency');
box on;
set(gca,'fontsize',fontsize);
set(gcf,'paperposition',[0 0 18 3]);
yticks(0:6:18);
print('prederror_histo_fofo_corrs.png','-dpng',res);
mrj_png_trans('prederror_histo_fofo_corrs.png');

figure(fig_handles(6));
xlabel('Prediction error (s)');
ylabel('Frequency');
box on;
set(gca,'fontsize',fontsize);
set(gcf,'paperposition',[0 0 18 3]);
yticks(0:6:18);
print('prederror_histo_fofo_eucs.png','-dpng',res);
mrj_png_trans('prederror_histo_fofo_eucs.png');



figure(fig_handles(7));
xlabel('Time (s)');
ylabel('Proportion "same"');
axis square; box on;
set(gca,'fontsize',fontsize);
print('windowed_fofo_predsbin.png','-dpng',res);
mrj_png_trans('windowed_fofo_predsbin.png');

figure(fig_handles(8));
xlabel('Time (s)');
ylabel('r-value (z'' transformed)');
axis square; box on;
set(gca,'fontsize',fontsize);
print('windowed_fofo_corrs.png','-dpng',res);
mrj_png_trans('windowed_fofo_corrs.png');

figure(fig_handles(9));
xlabel('Time (s)');
ylabel('Negative Euclidean dist');
axis square; box on;
set(gca,'fontsize',fontsize);
print('windowed_fofo_eucs.png','-dpng',res);
mrj_png_trans('windowed_fofo_eucs.png');



figure(fig_handles(13));
xlabel('False positive rate');
ylabel('True positive rate');
axis tight; box on;
set(gca,'fontsize',fontsize);
xticks(0:.2:1);
yticks(0:.2:1);
print('roc_curve_fofo_predsbin.png','-dpng',res);
mrj_png_trans('roc_curve_fofo_predsbin.png');

figure(fig_handles(14));
xlabel('False positive rate');
ylabel('True positive rate');
axis tight; box on;
set(gca,'fontsize',fontsize);
xticks(0:.2:1);
yticks(0:.2:1);
print('roc_curve_fofo_corrs.png','-dpng',res);
mrj_png_trans('roc_curve_fofo_corrs.png');

figure(fig_handles(15));
xlabel('False positive rate');
ylabel('True positive rate');
axis tight; box on;
set(gca,'fontsize',fontsize);
xticks(0:.2:1);
yticks(0:.2:1);
print('roc_curve_fofo_eucs.png','-dpng',res);
mrj_png_trans('roc_curve_fofo_eucs.png');
